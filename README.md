Script Python afin d'être prévenu de l'ouverture des réservations préscolaires sur le site espace-familles de la ville de Bezons.

La réservation de la restauration scolaire, accueils du matin, du soir, l'étude et les mercredis ainsi que les vacances se fait sur le site de la ville de Bezons. Mais nous ne sommes pas notifiées quand les réservations sont ouvertes/fermées.

Pour rappel, toute présence non réservée et/ou toute absence non justifiée feront l'objet d'une majoration. 

Je me suis donc bricolé ce script qui se connecte a mon compte et va récupérer le post-it en préambule du planning. Dans ce post-it, figure les informations sur les prochaines périodes de réservation. Si le post-it a changé depuis ma dernière connexion, alors je me l'envoi par email. 

Si ce script peut aider d'autres parents à ne pas manquer une réservation dans nos journées bien chargée, alors tant mieux. Une vérification toutes les 72h devrait être suffisante. Inutile d'établir des connexions plusieurs fois par jour.

Disclamer : Je publie la V.0 du script. Je ne l'ai pas testé au delà de quelques lancements manuel.
Disclamer 2 : Je ne suis pas Dev. Donc c'est surement très dirty pour un œil averti