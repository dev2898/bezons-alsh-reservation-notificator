#!/usr/bin/env python3

# Auteur: Vincent Brevard
# Date : 2024
# Quoi : Etre avertie de l'ouverture des réservations périscolaire

import sys
import requests
import subprocess
from bs4 import BeautifulSoup
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def send_mail(postits):
        # Paramètres du serveur SMTP
        smtp_server = 'your_smtp_server'
        port = 25
        sender_email = 'your_email'
        password = 'you_email_password'
        receiver_emails = ['dady@example.com', 'momy@example.com']

        # Création du message
        message = MIMEMultipart()
        message['From'] = sender_email
        message['To'] = ','.join(receiver_emails)
        message['Subject'] = 'ALSH Bezons Reservation'

        # Corps du message
        body = postits
        message.attach(MIMEText(body, 'html'))

        # Connexion au serveur SMTP
        with smtplib.SMTP(smtp_server, port) as server:
                #server.starttls()  # Activer le mode TLS (transport layer security)
                server.login(sender_email, password)
                text = message.as_string()
                server.sendmail(sender_email, receiver_emails, text)


def cnx(url, csrf_token, session):
        form_data = {
                "signin[username]": "Your_Bezons_espace_famille_login",
                "signin[password]": "Your_Bezons_espace_famille_password",
                "enregistrer": "Se+connecter",
                "signin[_csrf_token]": csrf_token
        }

        try:
                response = session.post(url, data=form_data, verify=False)
        except ConnectionError as e:
                #print("Erreur durant le login : ", e)
                message = "Execpt : Erreur durant le login : " + str(e)
                send_mail(message)

        if response.status_code != 200:
                #print("Echec de connexion : " , response.status_code)
                message = "Echec de connexion : " + str(response.status_code)
                send_mail(message)
                sys.exit()


def get_csrf_token(url, session):
        try:
                response = session.get(url , verify=False)
        except ConnectionError as e:
                #print("Erreur avec la page principale : ", e)
                message = "Erreur avec la page principale : " + str(e)
                send_mail(message)

        if response.status_code == 200:
                soup = BeautifulSoup(response.text, "html.parser")
                csrf_balise = soup.find(id="signin__csrf_token")
                if csrf_balise:
                        csrf_value = csrf_balise.get("value")
                        return str(csrf_value)
                else:
                        #print("Erreur CSRF non trouvé")
                        message = "Erreur CSRF non trouvé"
                        send_mail(message)
                        sys.exit()

        else:
                #print("Erreur lors du chargement de la page principale : " , response.status_code)
                message = "Erreur lors du chargement de la page principale : " + str(response.status_code)
                send_mail(message)
                sys.exit()


def get_planning_alsh(url, session):
        try:
                response = session.get(url, verify=False)
        except ConnectionError as e:
                #print("Erreur avec la page planning : ", e)
                message = "Erreur avec la page planning : " + str(e)
                send_mail(message)

        if response.status_code == 200:
                soup = BeautifulSoup(response.text, "html.parser")
                postits_balise = soup.find(id="postits")
                if postits_balise:
                        return str(postits_balise)
                else:
                        #print("Postits introuvable")
                        message = "Postits introuvable"
                        send_mail(message)
                        sys.exit()

        else:
                #print("Erreur lors du chargement de la page de planning : " , response.status_code)
                message = "Erreur lors du chargement de la page planning : " + str(response.status_code)
                send_mail(message)
                sys.exit()


def compare_postits(new_postits):
        command = "/usr/bin/touch /tmp/postits.html"
        subprocess.run(command, shell=True, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=5)

        f=open("/tmp/postits.html", "r")
        old_postits = f.read()
        f.close()

        if old_postits == new_postits:
                return True
        else:
                f=open("/tmp/postits.html", "w")
                f.write(str(new_postits))
                f.close()
                return False


if __name__ == '__main__':

        #Ignorer les erreurs ssl du site de Bezons
        requests.packages.urllib3.disable_warnings()

        # URL de la page de login
        url = 'https://espace-familles.ville-bezons.fr/guard/login'

        # gestion du token CSRF
        session = requests.Session()
        csrf_token = get_csrf_token(url, session)

        cnx(url, csrf_token, session)

        url = 'https://espace-familles.ville-bezons.fr/planning/alsh'
        new_postits = get_planning_alsh(url, session)
        if compare_postits(new_postits):
                print("Planning inchangé")
        else:
                print("le planning a changé")
                send_mail(new_postits)